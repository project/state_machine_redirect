<?php

namespace Drupal\state_machine_redirect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\csv_importer\ParserInterface;
use Drupal\csv_importer\Plugin\ImporterManager;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Class StateMachineRedirectConfigForm.
 *
 * @package Drupal\state_machine_redirect\Form
 */
class StateMachineRedirectConfigForm extends ConfigFormBase {



  /**
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The parser service.
   *
   * @var \Drupal\csv_importer\Parser\ParserInterface
   */
  protected $parser;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The importer plugin manager service.
   *
   * @var \Drupal\csv_importer\Plugin\ImporterManager
   */
  protected $importer;


  /**
   * The redirect path array.
   *
   * @var
   */
  private $state_redirect;


  /**
   * The routes of this website.
   *
   * @var
   */
  private $routes;

  /**
   * ImporterForm class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info service.
   * @param \Drupal\csv_importer\Parser\ParserInterface $parser
   *   The parser service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\csv_importer\Plugin\ImporterManager $importer
   *   The importer plugin manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteProviderInterface $route_provider, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_bundle_info, ParserInterface $parser, RendererInterface $renderer, ImporterManager $importer) {
    parent::__construct($config_factory);

    $this->routeProvider = $route_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->parser = $parser;
    $this->renderer = $renderer;
    $this->importer = $importer;

    // Set routes.
    $this->setRoutes();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.route_provider'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('csv_importer.parser'),
      $container->get('renderer'),
      $container->get('plugin.manager.importer')
    );

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'state_machine_redirect.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'state_machine_redirect_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('state_machine_redirect.settings');

    // Ajax routes not accessible.
    if ($form_state->isRebuilding()) {
      $this->setRoutes();
    }

    // Is ajax?
    $entity_type = $form_state->isRebuilding() ? $form_state->getValue('entity_type') : $config->get('entity_type');

    $form['state_machine_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'state_machine_container',
      ],
    ];

    $form['state_machine_container']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose entity type'),
      '#required' => TRUE,
      '#options' => $this->getEntityTypeOptions(),
      '#default_value' => $entity_type,
      "#empty_option" => "-None-",
      '#weight' => 0,
      '#ajax' => [
        'callback' => [$this, 'getContentEntityTypesAjaxForm'],
        'wrapper' => 'state_machine_container',
        'event' => 'change',
      ],
    ];

    if ($entity_type) {
      $options = $this->getEntityTypeBundleOptions($entity_type) ?: [$entity_type => $entity_type];
      // $config->get('entity_type_bundle');.
      if ($options) {

        // Is ajax?
        $bundle = $form_state->isRebuilding() ? $form_state->getValue('entity_type_bundle') : $config->get('entity_type_bundle');

        $form['state_machine_container']['bundle_container'] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => 'bundle_container',
          ],
        ];

        $form['state_machine_container']['bundle_container']['entity_type_bundle'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose entity bundle'),
          '#options' => $options,
          '#default_value' => $bundle,
          "#empty_option" => "-None-",
          '#required' => TRUE,
          '#weight' => 5,
          '#ajax' => [
            'callback' => [$this, 'getContentEntityBundleAjaxForm'],
            'wrapper' => "bundle_container",
            'event' => 'change',
          ],
        ];

      }

      $fields = $this->getEntityTypeFields($entity_type, $bundle);

      if ($bundle) {
        if (count($fields) > 0) {

          // Is ajax?
          $state_machine_field = $form_state->isRebuilding() ? $form_state->getValue('state_machine_field') : $config->get('state_machine_field');

          $form['state_machine_container']['bundle_container']['field_container'] = [
            '#type' => 'container',
            '#attributes' => [
              'id' => 'field_container',
            ],
            '#weight' => 7,

          ];

          $form['state_machine_container']['bundle_container']['state_machine_field'] = [
            '#type' => 'select',
            '#title' => $this->t('Choose State Machine Field'),
            '#options' => $fields,
            '#default_value' => $state_machine_field,
            "#empty_option" => "-None-",
            '#ajax' => [
              'callback' => [$this, 'getContentEntityFieldAjaxForm'],
              'wrapper' => "field_container",
              'event' => 'change',
            ],
            '#weight' => 6,
          ];

          if ($state_machine_field) {

            $states = $this->getStateFieldStates($entity_type, $bundle, $state_machine_field);
            // Foreach workflow state add an input field.
            foreach ($states as $state) {

              $key = implode("_", [$entity_type, $bundle, $state]);

              $default_route = isset($config->get('state_redirects')[$entity_type][$bundle]['state_redirects'][$state]) ? $config->get('state_redirects')[$entity_type][$bundle]['state_redirects'][$state] : FALSE;

              $form['state_machine_container']['bundle_container']['field_container'][$key] = [
                '#type' => 'select',
                '#title' => $this->t('Choose Redirect Route for state "@state" of bundle "@bundle"', ["@state" => $state, "@bundle" => $bundle]),
                '#options' => $this->routes,
                '#default_value' => $default_route,
                "#empty_option" => "-None-",
                '#weight' => 100,
              ];

            }
          }

        }

      }

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get entity type bundle options.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return array
   *   Entity type bundle options.
   */
  protected function getEntityTypeBundleOptions(string $entity_type) {
    $options = [];
    $entity = $this->entityTypeManager->getDefinition($entity_type);
    $type = $entity->getBundleEntityType();
    if ($entity && $type) {
      $types = $this->entityTypeManager->getStorage($type)->loadMultiple();

      if ($types && is_array($types)) {
        foreach ($types as $type) {
          $options[$type->id()] = $type->label();
        }
      }
    }

    return $options;
  }

  /**
   * Get entity type fields.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string|null $entity_type_bundle
   *   Entity type bundle.
   *
   * @return array
   *   Entity type fields.
   */
  protected function getEntityTypeFields(string $entity_type, string $entity_type_bundle = NULL) {
    $fields = [];

    if (!$entity_type_bundle) {
      $entity_type_bundle = key($this->entityBundleInfo->getBundleInfo($entity_type));
    }

    $entity_fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $entity_type_bundle);
    foreach ($entity_fields as $entity_field) {
      if ($entity_field->getType() == 'state') {
        $fields[$entity_field->getName()] = $entity_field->getName();
      }
    }

    return $fields;
  }

  /**
   * Get state field states.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string|null $entity_type_bundle
   *   Entity type bundle.
   *
   * @return array
   *   Entity type fields.
   */
  protected function getStateFieldStates(string $entity_type, string $entity_type_bundle = NULL, string $state_field) {

    $entity_fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $entity_type_bundle);

    $statefield = $entity_fields[$state_field];

    $entity_type_defenition = $this->entityTypeManager->getDefinition($entity_type);

    $workflows = $this->getWorkflows($entity_type_defenition, $state_field);

    $wf_id  = $statefield->getSettings()['workflow'];
    $states = array_keys($workflows[$wf_id]->getStates());
    // $states_assoc = array_combine($states,$states);.
    return $states;
  }

  /**
   * Gets the bundles for the current entity field.
   *
   * If the view has a non-exposed bundle filter, the bundles are taken from
   * there. Otherwise, the field's bundles are used.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The current entity type.
   * @param string $field_name
   *   The current field name.
   *
   * @return string[]
   *   The bundles.
   */
  protected function getBundles(EntityTypeInterface $entity_type, $field_name) {
    $bundles = [];
    $bundle_key = $entity_type->getKey('bundle');
    if ($bundle_key && isset($this->view->filter[$bundle_key])) {
      $filter = $this->view->filter[$bundle_key];
      if (!$filter->isExposed() && !empty($filter->value)) {
        // 'all' is added by Views and isn't a bundle.
        $bundles = array_diff($filter->value, ['all']);
      }
    }
    // Fallback to the list of bundles the field is attached to.
    if (empty($bundles)) {
      $map = $this->entityFieldManager->getFieldMap();
      $bundles = $map[$entity_type->id()][$field_name]['bundles'];
    }

    return $bundles;
  }

  /**
   * Gets the workflows used the current entity field.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The current entity type.
   * @param string $field_name
   *   The current field name.
   *
   * @return \Drupal\state_machine\Plugin\Workflow\WorkflowInterface[]
   *   The workflows.
   */
  protected function getWorkflows(EntityTypeInterface $entity_type, $field_name) {
    // Only the StateItem knows which workflow it's using. This requires us
    // to create an entity for each bundle in order to get the state field.
    $storage = $this->entityTypeManager->getStorage($entity_type->id());
    $bundles = $this->getBundles($entity_type, $field_name);
    $workflows = [];
    foreach ($bundles as $bundle) {
      $values = [];
      if ($bundle_key = $entity_type->getKey('bundle')) {
        $values[$bundle_key] = $bundle;
      }
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $storage->create($values);
      if ($entity->hasField($field_name)) {
        $workflow = $entity->get($field_name)->first()->getWorkflow();
        $workflows[$workflow->getId()] = $workflow;
      }
    }

    return $workflows;
  }

  /**
   * Entity type AJAX form handler.
   */
  public function getContentEntityTypesAjaxForm(array &$form, FormStateInterface $form_state) {

    return $form['state_machine_container'];
  }

  /**
   * Entity type AJAX form handler.
   */
  public function getContentEntityBundleAjaxForm(array &$form, FormStateInterface $form_state) {

    return $form['state_machine_container']['bundle_container'];
  }

  /**
   * Entity type AJAX form handler.
   */
  public function getContentEntityFieldAjaxForm(array &$form, FormStateInterface $form_state) {

    return $form['state_machine_container']['bundle_container']['field_container'];
  }

  /**
   * Get entity type options.
   *
   * @return array
   *   Entity type options.
   */
  protected function getEntityTypeOptions() {
    $options = [];
    $plugin_definitions = $this->importer->getDefinitions();

    // Only User is supported at this stage.
    foreach ($plugin_definitions as $definition) {
      $entity_type = $definition['entity_type'];
      if ($this->entityTypeManager->hasDefinition($entity_type) && $entity_type == 'user') {
        $entity = $this->entityTypeManager->getDefinition($entity_type);
        $options[$entity_type] = $entity->getLabel();
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $redirect_path = $form_state->getValue('login_redirect');

    if (!empty($redirect_path)) {
      $url = $this->pathValidator->getUrlIfValidWithoutAccessCheck($redirect_path);

      if (!$url) {
        $form_state->setErrorByName('login_redirect', 'Redirect path must exist!');
      }
    }
  }

  /**
   * Set all website routes as a associative array.
   */
  public function setRoutes() {

    // Exclude routes from the list with no-route meaning.
    $blacklist = [
      '<current>',
      '<none>',
      '<nolink>',
    ];

    $routes = [];

    foreach ($this->routeProvider->getAllRoutes() as $name => $route) {
      $path = $route->getPath();
      if (in_array($name, $blacklist, TRUE)) {
        continue;
      }

      $routes[] = $name;

    }

    // Associative array.
    $this->routes = array_combine($routes, $routes);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $entity_type = $form_state->getValue('entity_type');
    $entity_type_bundle = $form_state->getValue('entity_type_bundle');
    $state_machine_field = $form_state->getValue('state_machine_field');

    $this->state_redirect[$entity_type] = [];
    $this->state_redirect[$entity_type][$entity_type_bundle] = [];
    $this->state_redirect[$entity_type][$entity_type_bundle]["state_machine_field"] = $state_machine_field;
    $this->state_redirect[$entity_type][$entity_type_bundle]["state_redirects"] = [];

    if ($entity_type && $entity_type_bundle && $state_machine_field) {
      $states = $this->getStateFieldStates($entity_type, $entity_type_bundle, $state_machine_field);
      // Foreach workflow state add an input field.
      foreach ($states as $state) {

        $key = implode("_", [$entity_type, $entity_type_bundle, $state]);

        $value = $form_state->getValue($key);
        if ($value) {
          $this->state_redirect[$entity_type][$entity_type_bundle]["state_redirects"][$state] = $value;
        }
      }
    }

    $this->config('state_machine_redirect.settings')
      // Redirect save.
      ->set('state_redirects', $this->state_redirect)
      ->set('entity_type', $entity_type)
      ->set('entity_type_bundle', $entity_type_bundle)
      ->set('state_machine_field', $state_machine_field)
      ->save();
  }

}
