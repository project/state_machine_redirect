<?php

namespace Drupal\state_machine_redirect\Routing;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\chamber_auth\Routing
 */
class RouteSubscriber implements EventSubscriberInterface {

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * Redirects.
   */
  protected $state_redirects;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxyInterface $account_proxy) {

    $this->accountProxy = $account_proxy;

    $config = \Drupal::config('state_machine_redirect.settings');

    $this->state_redirects = $config->get('state_redirects');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   *
   */
  public function checkForRedirection(GetResponseEvent $event) {

    // In order to get the $route you probably should use the $route_match.
    $route = \Drupal::routeMatch()->getRouteObject();
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);
    $route_name = \Drupal::routeMatch()->getRouteName();

    // Match user paths -login, logout, account.
    $is_user = strstr($route_name, "user.");

    $is_system = strstr($route_name, "system.");

    $accountProxy = $this->accountProxy;
    $account = $accountProxy->getAccount();

    if ($account->isAuthenticated() &&
      !$is_system &&
      !$is_admin &&
      !$is_user) {

      // Check if redirect defined for state.
      $redirect = $this->getRedirectPath();

      // Exclude redirect loops.
      if ($redirect !== $route_name) {

        if ($redirect) {
          $url = Url::fromRoute($redirect)->setAbsolute()->toString();

          $event->setResponse(
            new RedirectResponse($url, 301)
          );
        }
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForRedirection');
    return $events;
  }

  /**
   * Implements hook_user_login().
   */
  public function getRedirectPath() {

    $redirect = FALSE;
    $accountProxy = $this->accountProxy;
    $account = $accountProxy->getAccount();
    $user = User::load($account->id());

    // Get the bundle from configuration
    // get the state field from configuration.
    $config = $this->state_redirects;
    $entity_type = $entity_bundle = $state_machine_field = $state_redirects = FALSE;

    foreach ($config as $type => $config_bundle) {
      foreach ($config_bundle as $bundle => $config_field) {
        $entity_type = $type;
        $entity_bundle = $bundle;
        $state_machine_field = $config_field['state_machine_field'];
        $state_redirects = $config_field['state_redirects'];
      }
    }

    if ($user->getEntityTypeId() == $entity_type && $user->bundle() == $entity_bundle && $user->hasField($state_machine_field)) {

      $state = $user->get($state_machine_field)->first()->value;

      $redirect = isset($state_redirects[$state]) ? $state_redirects[$state] : FALSE;
    }

    return $redirect;

  }

}
